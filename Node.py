class Node:
    state = None
    parent = None

    def __init__(self, state=None, parent=None):
        self.state = state
        self.parent = parent

    def showCompletePath(self, problem):
        tmpNode = self
        seq = []
        while tmpNode is not None:
            seq.append(tmpNode)
            tmpNode = tmpNode.parent
        seq.reverse()
        for elem in seq:
            problem.showNodeState(elem)
            print('_' * 100)
