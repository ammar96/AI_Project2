import copy
import random

from Node import Node

class N_Queens():
    n = None
    initialState = None

    def __init__(self, n=8):
        self.n = n
        self.initialState = random.sample(range(n), n)

    def goalTest(self, node):
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if abs(node.state[i] - node.state[j]) == abs(i - j):
                    return False
        return True

    def getFitness(self, node):
        counter = 0
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if abs(node.state[i] - node.state[j]) == abs(i - j):
                    counter += 1
        if counter == 0:
            return float('inf')
        return 1 / counter

    def getStandardizedFitness(self, node):
        return self.getFitness(node)


    def generateInitialNode(self):
        return Node(self.initialState)

    def showNodeState(self, node):
        print(node.state)

    def generateAllSuccessors(self, node):  # select two indexes and exchange their elements
        successors = []
        for i in range(self.n):
            for j in range(i + 1, self.n):
                tmpNode = copy.deepcopy(node)
                tmpNode.state[i], tmpNode.state[j] = tmpNode.state[j], tmpNode.state[i]
                successors.append(tmpNode)
        return successors

    def getRandomSuccessor(self, node):
        indices = random.sample(range(self.n), 2)
        suc = copy.deepcopy(node)
        suc.state[indices[0]], suc.state[indices[1]] = suc.state[indices[1]], suc.state[indices[0]]
        return suc

    def getImprovedSuccessors(self, node):  # also returns number of all generated successors
        successors = self.generateAllSuccessors(node)
        numAllSucs = len(successors)
        if successors is None:
            return None
        i = len(successors) - 1
        while i >= 0:
            if self.getStandardizedFitness(successors[i]) <= self.getStandardizedFitness(node):
                successors.pop(i)
            i -= 1
        return [successors, numAllSucs]

