import math
import random

from Equation import Equation
from Suduko import Suduko
from N_Queens import N_Queens
from GraphPart import GraphPart

numGeneratedNodes = numExpandedNodes = 0
actions = 0


def doSimpleHC_search(problem, silentMode=False):
    return doGeneralHillClimbing_search(problem, simpleHC_selectionFunc, silentMode)


def doStochasticHC_search(problem):
    return doGeneralHillClimbing_search(problem, stochasticHC_selectionFunc)


def doFirstChoiceHC_search(problem):
    return doGeneralHillClimbing_search(problem, firstChoiceHC_selectionFunc)


def doGeneralHillClimbing_search(problem, selectNextNode, silentMode=False):
    global numGeneratedNodes, numExpandedNodes, actions
    current = problem.generateInitialNode()
    numGeneratedNodes += 1
    while True:
        if problem.goalTest(current):
            if not silentMode:
                print('optimal solution found!')
            return current
        next = selectNextNode(problem, current)
        actions += 1
        if next is None:
            if not silentMode:
                print('this is the best found solution:')
            return current
        current = next


def simpleHC_selectionFunc(problem, current):
    global numExpandedNodes, numGeneratedNodes
    numExpandedNodes += 1
    successors = problem.getImprovedSuccessors(current)[0]
    numGeneratedNodes += problem.getImprovedSuccessors(current)[1]
    if successors is None:
        return None
    tmpMax = -float('inf')
    next = None
    for s in successors:
        if problem.getStandardizedFitness(s) > tmpMax:
            tmpMax = problem.getStandardizedFitness(s)
            next = s
    return next


def stochasticHC_selectionFunc(problem, node):
    global numExpandedNodes, numGeneratedNodes
    numExpandedNodes += 1
    successors = problem.getImprovedSuccessors(node)[0]
    numGeneratedNodes += problem.getImprovedSuccessors(node)[1]
    try:
        return successors[random.sample(range(len(successors)), 1)[0]]
    except ValueError:
        return None


def firstChoiceHC_selectionFunc(problem, node):
    global numGeneratedNodes, numExpandedNodes
    numExpandedNodes += 1
    numTries = 20
    for i in range(numTries):
        suc = problem.getRandomSuccessor(node)
        numGeneratedNodes += 1
        if problem.getFitness(suc) > problem.getFitness(node):
            return suc
    return None


def doRandomRestartHC_search(problem, iterations=10):
    solution = None
    tmpMax = -float('inf')
    for i in range(iterations):
        tmpSol = doSimpleHC_search(problem, True)
        if problem.getStandardizedFitness(tmpSol) > tmpMax:
            solution = tmpSol
            tmpMax = problem.getStandardizedFitness(solution)
    return solution


def doGeneralSA_search(problem, schedule):  # T will approach to zero as time passes
    global numGeneratedNodes, numExpandedNodes, actions
    current = problem.generateInitialNode()
    numGeneratedNodes += 1
    t = 1
    while True:
        actions += 1
        T = schedule(t)
        if T <= math.pow(10, -5):  # it's almost zero.
            return current
        next = problem.getRandomSuccessor(current)
        numGeneratedNodes += 1
        deltaE = problem.getStandardizedFitness(next) - problem.getStandardizedFitness(current)
        if deltaE > 0:
            numExpandedNodes += 1
            current = next
        elif random.random() < math.exp(deltaE / T):
            current = next
            numExpandedNodes += 1
        t += 1


def linearMultiplicativeCooler(t):
    T0, alpha = 100, 1000
    return T0 / (1 + alpha * t)


def logMultiplicativeCooler(t):  # this method was weaker for TSP
    T0, alpha = 1, math.pow(10, 5)
    return T0 / (1 + alpha * math.log(1 + t, 10))


def expMultiplicativeCooler(t):
    T0, alpha = 1, .85
    return T0 * math.pow(alpha, t)


def generateInitialPop(problem, popSize):
    pop = []
    for i in range(popSize):
        node = problem.generateInitialNode()
        pop.append(node)
    return pop


def sortPop_descending(problem, pop):
    quickSortHelper(problem, pop, 0, len(pop) - 1)
    pop.reverse()


def quickSortHelper(problem, pop, first, last):
    if first < last:
        splitpoint = partition(problem, pop, first, last)
        quickSortHelper(problem, pop, first, splitpoint - 1)
        quickSortHelper(problem, pop, splitpoint + 1, last)


def partition(problem, pop, first, last):
    pivotvalue = problem.getFitness(pop[first])
    leftmark = first + 1
    rightmark = last
    done = False
    while not done:
        while leftmark <= rightmark and problem.getFitness(pop[leftmark]) <= pivotvalue:
            leftmark += 1
        while problem.getFitness(pop[rightmark]) >= pivotvalue and rightmark >= leftmark:
            rightmark -= 1
        if rightmark < leftmark:
            done = True
        else:
            temp = pop[leftmark]
            pop[leftmark] = pop[rightmark]
            pop[rightmark] = temp
    temp = pop[first]
    pop[first] = pop[rightmark]
    pop[rightmark] = temp
    return rightmark


def selectOneParent_rankSelection(pop):  # the population is supposed to be sorted descendingly.
    totalLength = len(pop) * (len(pop) + 1) / 2
    randValue = random.random() * totalLength
    for i in range(len(pop)):
        randValue -= i + 1
        if randValue <= 0:
            return pop[i - 1]


# TODO: can two parents be the same?, if yes check for it in the following function.
def selectCouples(pop, popSize):
    couples = []
    for i in range(popSize):
        p1 = selectOneParent_rankSelection(pop)
        p2 = selectOneParent_rankSelection(pop)
        couples.append([p1, p2])
    return couples


def solveUsingGenAlgo(problem, n=2, p=-1.0, popSize=20, iterations=10):
    if p == -1:
        p = 1 / n
    pop = generateInitialPop(problem, popSize)
    sortPop_descending(problem, pop)
    for i in range(iterations):
        offsprings = []
        if problem.goalTest(pop[0]):
            print('optimal solution found!')
            return pop[0]
        couples = selectCouples(pop, popSize)
        for c in couples:
            offsprings.append(problem.doCrossover(c[0], c[1]))
        mutatedOffsprings = []
        for node in offsprings:
            doGaussMutation(node, 0, .5)
            mutatedOffsprings.append(node)
        pop = mergeLists(pop, mutatedOffsprings)
        sortPop_descending(problem, pop)
        pop = selNewPop(problem, pop, popSize)
        details = getPopFitnessDetails(problem, pop)
        print('details of generation number ', i + 1, ':')
        print('the worst node:')
        problem.showNodeState(details[0])
        print('the best node:')
        problem.showNodeState(details[1])
        print('and average fitness of this generation is', details[2])
        print('_' * 100)
    print('this is the best found solution after ', iterations, ' iterations!')
    return pop[0]


def mergeLists(l1, l2):
    res = []
    for e in l1:
        res.append(e)
    for e in l2:
        res.append(e)
    return res


def getPopFitnessDetails(problem,
                         pop):  # return worst node, best node and average fitness of the population respectively.
    sum = problem.getFitness(pop[0])
    bestNode = worstNode = pop[0]
    i = 1
    while i < len(pop):
        sum += problem.getFitness(pop[i])
        if problem.getFitness(pop[i]) > problem.getFitness(bestNode):
            bestNode = pop[i]
        elif problem.getFitness(pop[i]) < problem.getFitness(worstNode):
            worstNode = pop[i]
        i += 1
    return [worstNode, bestNode, sum / len(pop)]


def doTheMutation(p, node):
    for i in range(len(node.state)):
        if random.random() < p:
            node.state[i] = 1 - node.state[i]


def doGaussMutation(node, mean, sdev):
    d = len(node.state)
    for i in range(d):
        node.state[i] += random.normalvariate(mean, sdev)
        if node.state[i] < -10:
            node.state[i] = -10
        elif node.state[i] > 10:
            node.state[i] = 10


def selNewPop(problem, nodes, popSize):
    selectedPop = []
    sortPop_descending(problem, nodes)
    i = 0
    top = popSize
    while i < top:
        if not problem.isValid(nodes[i]):  # skip invalid nodes
            top += 1
        else:
            selectedPop.append(nodes[i])
        i += 1
        if i == len(nodes):
            return selectedPop
    return selectedPop


def showSearchDetails():
    print('number of generated nodes: ', numGeneratedNodes)
    print('number of expanded nodes: ', numExpandedNodes)

# nq = N_Queens(6)
# nq.showNodeState(doRandomRestartHC_search(nq))
# showSearchDetails()
# tsp = TSP()
# tsp.showNodeState(doSimpleHC_search(tsp))
# showSearchDetails()
# knapsack = Knapsack()
# knapsack.showNodeState(solveUsingGenAlgo(knapsack, 3, -1, 100, 20))
# eq = Equation()
# eq.showNodeState(solveUsingGenAlgo(eq, popSize=20, iterations=100, d=30))
# suduko = Suduko()
# suduko.showNodeState(doGeneralSA_search(suduko, logMultiplicativeCooler))
# showSearchDetails()
# print('actions: ', actions)
# eq = Equation()
# eq.showNodeState(solveUsingGenAlgo(eq, 3, -1, 100, 20))
gp = GraphPart()
gp.showNodeState(doGeneralSA_search(gp, linearMultiplicativeCooler))