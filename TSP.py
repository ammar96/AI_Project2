import random
import copy
import numpy as np
from Node import Node


class TSP:
    n = None
    map = None
    cityNames = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E'}

    def __init__(self, n=5):
        self.readMapFromInput()

        # self.map = [[0, 10, 5, 7, 4], [10, 0, 2, 3, 7], [5, 2, 0, 8, 8],
        #             [7, 3, 8, 0, 5], [4, 7, 8, 5, 0]]

    def readMapFromInput(self):
        print('enter the number of cities and map:')
        self.n = int(input())
        self.map = [[0 for x in range(self.n)] for y in range(self.n)]
        for i in range(self.n):
            row = input()
            for j in range(self.n):
                self.map[i][j] = int(row.split()[j])

    def generateAllSuccessors(self, node):  # select two indexes and exchange their elements
        successors = []
        for i in range(self.n):
            for j in range(i + 1, self.n):
                tmpNode = copy.deepcopy(node)
                tmpNode.state[i], tmpNode.state[j] = tmpNode.state[j], tmpNode.state[i]
                successors.append(tmpNode)
        return successors

    def getRandomSuccessor(self, node):
        indices = random.sample(range(self.n), 2)
        suc = copy.deepcopy(node)
        suc.state[indices[0]], suc.state[indices[1]] = suc.state[indices[1]], suc.state[indices[0]]
        return suc

    def getImprovedSuccessors(self, node):  # also returns number of all generated successors
        successors = self.generateAllSuccessors(node)
        numAllSucs = len(successors)
        if successors is None:
            return None
        i = len(successors) - 1
        while i >= 0:
            if self.getStandardizedFitness(successors[i]) <= self.getStandardizedFitness(node):
                successors.pop(i)
            i -= 1
        return [successors, numAllSucs]

    def getCost(self, node):
        sum = 0
        for i in range(self.n):
            sum += self.map[node.state[i] % self.n][node.state[(i + 1) % self.n] % self.n]
        return sum

    def getFitness(self, node):
        return 1 / self.getCost(node)

    def getStandardizedFitness(self, node):  # fitness should always lie between zero and one.
        return self.getFitness(node)

    def goalTest(self, node):
        return False

    def generateInitialNode(self):
        return Node(random.sample(range(self.n), self.n))

    def showNodeState(self, node):
        for i in range(self.n):
            print(self.cityNames[node.state[i]], end=' ')
        print('\nthe cost of this solution is: ', self.getCost(node))
