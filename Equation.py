import random

from Node import Node


class Equation:
    n = 4

    def getFitness(self, node):
        vars = node.state
        return abs(1 / (40 - (vars[0] + 2 * vars[1] + 3 * vars[2] + 4 * vars[3]) + 1))

    def generateInitialNode(self):
        state = []
        for i in range(self.n):
            state.append(random.random() * 10)
        return Node(state)

    def goalTest(self, node):
        vars = node.state
        return vars[0] + 2 * vars[1] + 3 * vars[2] + 4 * vars[3] == 40

    def showNodeState(self, node):
        print(node.state, 'and the fitness of this node is: ', self.getFitness(node))

    def doCrossover(self, parent1, parent2):
        newState = []
        d = len(parent1.state)
        for i in range(d):
            newState.append((parent1.state[i] + parent2.state[i]) / 2)
        return Node(newState)

    def isValid(self, node):
        return True
