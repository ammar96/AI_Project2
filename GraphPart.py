import copy
import random

from Node import Node


class GraphPart:
    graph = None

    def getFitness(self, node):
        numVerticesDiff = abs(len(node.state[0]) - len(node.state[1]))
        numRemovedEdges = 0
        for i in node.state[0]:
            for j in node.state[1]:
                # check if i was connected to j
                if i in self.graph[j]:
                    numRemovedEdges += 1
        return 1 / (numVerticesDiff + numRemovedEdges + 1)

    def generateInitialNode(self):
        r0 = [1, 2]
        r1 = [0, 2, 3]
        r2 = [0, 1, 4, 5]
        r3 = [1, 4, 11]
        r4 = [2, 3, 10]
        r5 = [2, 6]
        r6 = [5, 7, 8, 10]
        r7 = [6, 8]
        r8 = [6, 7, 9]
        r9 = [8, 10]
        r10 = [4, 6, 9, 11]
        r11 = [3, 10]
        self.graph = [r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11]

        return Node([[0, 1, 2, 3, 4], [5, 6, 7, 8, 9, 10, 11]])

    def goalTest(self, node):
        return False

    def showNodeState(self, node):
        print(node.state, 'and the fitness of this node is: ', self.getFitness(node))

    def getRandomSuccessor(self, node):
        tmpNode = copy.deepcopy(node)
        if len(tmpNode.state[0]) == 1:
            randChoice = True
        elif len(tmpNode.state[1]) == 1:
            randChoice = False
        else:
            randChoice = random.choice([False, True])
        if randChoice:
            index = random.randrange(0, len(tmpNode.state[1]))
            tmpNode.state[0].append(tmpNode.state[1][index])
            tmpNode.state[0] = sorted(tmpNode.state[0])
            del tmpNode.state[1][index]
        else:
            index = random.randrange(0, len(tmpNode.state[0]))
            tmpNode.state[1].append(tmpNode.state[0][index])
            tmpNode.state[1] = sorted(tmpNode.state[1])
            del tmpNode.state[0][index]
        return tmpNode

    def getStandardizedFitness(self, node):
        return self.getFitness(node)
