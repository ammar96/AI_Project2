import copy
import random

from Node import Node


class Suduko:
    def getFitness(self, node):
        if self.countSumConf(node) == 0:
            return float('inf')
        return 1 / self.countSumConf(node)

    def countSumConf(self, node):
        return self.countRowsConf(node) + self.countColsConf(node) + self.countCellsConf(node)

    def countRowsConf(self, node):
        counter = 0
        for i in range(4):
            counter += self.countRowConf(node, i)
        return counter

    def countRowConf(self, node, row):
        counter = 0
        for i in range(1, 5):
            if i not in node.state[row]:
                counter += 1
        if counter > 0:
            print('conf in row ', row)
            exit(-2)
        return counter

    def countColsConf(self, node):
        counter = 0
        for i in range(4):
            counter += self.countColConf(node, i)
        return counter

    def countColConf(self, node, col):
        myCol = []
        for j in range(4):
            myCol.append(node.state[j][col])

        counter = 0
        for i in range(1, 5):
            if i not in myCol:
                counter += 1
        return counter

    def countCellsConf(self, node):
        cell0 = [node.state[0][0], node.state[0][1], node.state[1][0], node.state[1][1]]
        cell1 = [node.state[0][2], node.state[0][3], node.state[1][2], node.state[1][3]]
        cell2 = [node.state[2][0], node.state[2][1], node.state[3][0], node.state[3][1]]
        cell3 = [node.state[2][2], node.state[2][3], node.state[3][2], node.state[3][3]]
        return self.getConf(cell0) + self.getConf(cell1) + self.getConf(cell2) + self.getConf(cell3)

    def getConf(self, list):
        counter = 0
        for i in range(1, 5):
            if i not in list:
                counter += 1
        return counter

    def generateInitialNode(self):
        r0 = [2, 3, 4, 1]
        r1 = [2, 1, 3, 4]
        r2 = [1, 2, 4, 3]
        r3 = [4, 1, 2, 3]
        return Node([r0, r1, r2, r3])

    def goalTest(self, node):
        return self.countSumConf(node) == 0

    def showNodeState(self, node):
        print(node.state, 'and the fitness of this node is: ', self.getFitness(node))

    def generateAllSuccessors(self, node):  # select two indexes and exchange their elements
        successors = []
        for row in range(4):
            for i1 in range(4):
                for i2 in range(4):
                    if i1 == i2:
                        continue
                    tmpNode = copy.deepcopy(node)
                    tmpNode.state[row][i1], tmpNode.state[row][i2] = tmpNode.state[row][i2], tmpNode.state[row][i1]
                    successors.append(tmpNode)
        return successors

    def getRandomSuccessor(self, node):
        row = random.sample(range(4), 1)[0]
        i1 = random.sample(range(4), 1)[0]
        i2 = random.sample(range(4), 1)[0]
        while i1 == i2:
            i2 = random.sample(range(4), 1)[0]
        tmpNode = copy.deepcopy(node)
        tmpNode.state[row][i1], tmpNode.state[row][i2] = tmpNode.state[row][i1], tmpNode.state[row][i2]
        return tmpNode

    def getImprovedSuccessors(self, node):  # also returns number of all generated successors
        successors = self.generateAllSuccessors(node)
        numAllSucs = len(successors)
        if successors is None:
            return None
        i = len(successors) - 1
        while i >= 0:
            if self.getStandardizedFitness(successors[i]) <= self.getStandardizedFitness(node):
                successors.pop(i)
            i -= 1
        return [successors, numAllSucs]

    def getStandardizedFitness(self, node):
        return self.getFitness(node)
