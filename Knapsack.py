import random

from Node import Node


class Knapsack:
    n = None
    items = []
    capacity = None

    def __init__(self):
        self.readItemsFromInput()
        # self.n = 5
        # self.items.append(self.Item(10, 12))
        # self.items.append(self.Item(12, 14))
        # self.items.append(self.Item(3, 9))
        # self.items.append(self.Item(6, 12))
        # self.items.append(self.Item(9, 14))
        # self.capacity = 25

    def readItemsFromInput(self):
        print('enter problem details:')
        self.n = int(input())
        weights = []
        line = input()
        for i in range(self.n):
            weights.append(int(line.split()[i]))
        line = input().split()
        for i in range(self.n):
            self.items.append(self.Item(weights[i], int(line[i])))
        self.capacity = int(input())

    def getFitness(self, node):
        sum = 0
        for i in range(self.n):
            sum += node.state[i] * self.items[i].value
        node.test = sum
        return sum

    def getTotalWeight(self, node):
        sum = 0
        for i in range(self.n):
            sum += node.state[i] * self.items[i].weight
        node.test2 = sum
        return sum

    def isValid(self, node):
        return self.getTotalWeight(node) <= self.capacity

    def generateInitialNode(self):
        node = Node(random.sample([0] * self.n + [1] * self.n, self.n))
        # while not self.isValid(node):
        # node = Node(random.sample([0] * self.n + [1] * self.n, self.n))
        return node

    def goalTest(self, node):
        return False

    def showNodeState(self, node):
        print(node.state, 'and the fitness of this node is: ', self.getFitness(node))

    def doCrossover(self, parent1, parent2, n):
        state = []
        nPoints = random.sample(range(len(parent1.state)), n)
        parentNum = 0
        for i in range(len(parent1.state)):
            if parentNum == 0:
                state.append(parent1.state[i])
            else:
                state.append(parent2.state[i])
            if i in nPoints:
                i = 1 - i
        return Node(state)

    class Item:
        value = None
        weight = None

        def __init__(self, weight, value):
            self.value = value
            self.weight = weight
